#Simple update script
This is a simple solution to update your application via git

```javascript
var updateSys = require('./lib/updateSys');

updateSys.autoUpdate(errorCallback, updateCallback);

function errorCallback(err) {
  console.warn(err);
}
function updateCallback(isUpdated) {
  if(isUpdated) {
    console.warn("updated the code, restart required");
  }
  else {
    console.log("no update found");
  }
}
```