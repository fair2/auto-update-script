// REQUIRES: simple-git
// npm install simple git -s

const git = require('simple-git');

class UpdateSys {
  constructor() {
    this.remoteUrl = "";
    this.gitBranch = "master";
    this.useGit = true;
  }
  setUseGit(useGit) {
    this.useGit = useGit;
  }
  setUsername(username) {
    this.userName = username;
    this.generateUrl();
  }
  setPassword(pass) {
    this.pass = pass;
    this.generateUrl();
  }
  setRepo(repo) {
    this.repo = repo;
    this.generateUrl();
  }
  setGitBranch(gitBranch) {
    this.gitBranch = gitBranch;
  }
  setRemoteUrl(remoteUrl) {
    this.remoteUrl = remoteUrl;
  }
  generateUrl() {
    if (this.repo === undefined) {
      return;
    }
    this.remoteUrl = "https://";
    if (this.pass !== undefined && this.userName !== undefined) {
      this.remoteUrl += this.pass + ":";
    }
    if (this.userName !== undefined) {
      this.remoteUrl += this.userName + "@";
    }
    this.remoteUrl += this.repo;
  }
  autoUpdate(callback, errorCallback) {
    if (typeof (callback) == "function") {
      this.callback = callback;
    }
    else {
      this.callback = function () { };
    }
    if (typeof (errorCallback) == "function") {
      this.errorCallback = errorCallback;
    }
    else {
      this.errorCallback = function () { };
    }
    if(this.useGit) {
      this.updateCode(this.errorCallback, this.updateDependencies);
    }
    else {
      this.updateDependencies(errorCallback, callback);
    }
  }
  updateCode(error, callback) {
    if (this.remoteUrl == "") {
      git().pull(this.gitPullCallback);
    }
    else {
      git().pull(this.remoteUrl, this.gitBranch, this.gitPullCallback);
    }
  }
  gitPullCallback(err, update) {
    if (err !== null) {
      if (typeof (err) == "function") {
        error.bind(this)(err);
      }
      return;
    }
    if (update.summary.changes > 0) {
      if (typeof (callback) == "function") {
        //console.log("updated!")
        callback.bind(this)(true); // Successful pulled new code, app will need to be restarted!
      }
    }
    else {
      if (typeof (callback) == "function") {
        callback.bind(this)(false);
      }
    }
  }
  updateDependencies(error, callback) {
    if (typeof (error) == "function") {
      var cmd = true;
    }
    else {
      var cmd = error;
      error = this.error;
      callback = this.callback;
    }
    // if (cmd == false) {
    //   callback(false);
    //   return;
    // }
    var exec = require('child_process').exec;
    var command = "npm install"; // && npm update
    exec(command,
      function (error, stdout, stderr) {
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        if (error !== null) {
          console.log('exec error: ' + error);
          if (typeof (error) == "function") {
            error(false);
          }
        }
        else {
          if (typeof (callback) == "function") {
            callback(true);
          }
        }
      });
  }
}

module.exports = new UpdateSys();

// new UpdateSys().autoUpdate(callback, errorCallback);
// function errorCallback(val) {

// }
// function callback(val) {
//   // restart app
// }

// EXAMPLE CODE
// var updateSys = require('./lib/updateSys');

// updateSys.autoUpdate(errorCallback, updateCallback);

// function errorCallback(err) {
//   console.warn(err);
// }
// function updateCallback(isUpdated) {
//   if(isUpdated) {
//     console.warn("updated the code, restart required");
//   }
//   else {
//     console.log("no update found");
//   }
// }